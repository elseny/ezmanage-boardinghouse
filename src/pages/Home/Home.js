import React from 'react'
import PlacesCard from '../../shared/components/PlacesCard/PlacesCard';
import PlacesData from '../../shared/assets/dummyData/places.json';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    gridContainerMargin: {
      margin:'15px',
    },
});

function Home() {

    const classes = useStyles();

    return (
        <Grid container justify="center">
            <Grid item container justify="center" xs={11}  spacing={2} className={classes.gridContainerMargin}>
                {
                    PlacesData.map(place => {
                        return <Grid item xs={12} sm={4}>
                            <PlacesCard place={place}/>
                        </Grid>
                    })
                }
            </Grid>
        </Grid>
    )
}

export default Home
