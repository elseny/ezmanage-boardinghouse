import React,{useState} from 'react'

//mui
import Grid from '@material-ui/core/Grid';
import { makeStyles,useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import EzmeinLogo from "../../shared/assets/images/Ezmein Program Kost.jpg";
import HouseIcon from '@material-ui/icons/House';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import AccessibilityNewIcon from '@material-ui/icons/AccessibilityNew';
import Card from '@material-ui/core/Card';
import Link from '@material-ui/core/Link';
import {Typography, TextField, Button } from '@material-ui/core';
import BackgroundImage from '../../shared/assets/images/LoginBackground.jpg'

//react-router
import {useHistory} from 'react-router-dom';

//assets

const useStyles = makeStyles(theme => ({
    container:{
        height:'100%',
    },
    leftContainer:{
        [theme.breakpoints.down('sm')]:{
            paddingBottom:'10px'
        },
        paddingBottom:0,
        // background: 'linear-gradient(130.27deg,#d9a7a0 4.55%,#bd6768 55.81%, #a12426 95.81%)',
        backgroundColor:'#a12426',
    },
    logo:{
        height:'200px',
    },
    ezManageTypo:{
        fontSize:'30px',
        fontFamily: [
            'Cormorant',
            'cursive',
          ].join(','),
        [theme.breakpoints.down('sm')]: {
            fontSize:'18px',
        },  
        [theme.breakpoints.between('sm','md')]: {
            fontSize:'20px',
        },  
    },
    materialIcon:{
        margin:'0px 5px 0px 0px',
        fontSize:'40px',
        [theme.breakpoints.down('sm')]: {
            fontSize:'30px',
        },  
    },
    textColorWhite:{
        color:'white',
    },
    formTextField:{
        backgroundColor:'#010d47',
        borderRadius:'15px',
        '& label': {
            color: 'white',
        },
        '& label.Mui-focused': {
            color: '#aa3b3c',
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: 'white',
            },
            '&.Mui-focused fieldset': {
                borderColor: '#aa3b3c',
            },
        },
    },
    loginCard:{
        width:'80%',
        borderRadius:'15px',
        boxShadow: '2px 0px 5px 5px gray',
        backgroundColor:'#010d47',
    },
    rightContainer:props=>({
        paddingTop: props.sizeXs?'10px':0,
        paddingBottom:props.sizeXs?'20px':0,
        backgroundImage:`url(${BackgroundImage})`,
    }),
    bottomSvgStyle:{
        width: '58.3%',
        position:'fixed',
        strokeDasharray: '5px',
        bottom:0,
        opacity:0.8,
        strokeWidth:'2px',
        [theme.breakpoints.down('xs')]: {
            width:'100%',
        }
    },
    topSvgStyle:{
        width: '58.3%',
        height:'100%',
        position:'absolute',
        strokeDasharray: '5px',
        opacity:0.8,
        strokeWidth:'2px',
        [theme.breakpoints.down('xs')]: {
            width:'100%',
        }
    }
}));

function Login() {

    const theme = useTheme();
    const matchedSm = useMediaQuery(theme.breakpoints.down('xs'));
    const classes = useStyles({sizeXs:matchedSm});
    let history = useHistory();

    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [name,setName] = useState("");
    const [registerMode,setRegisterMode] = useState(false);

    const handleEmail = (e) =>{
        setEmail(e.target.value);
    }

    const handlePassword = (e) => {
        setPassword(e.target.value);
    }

    const handleRegisterMode = () => {
        setRegisterMode(state=>!state)
    }

    const handleName = (e) => {
        setName(e.target.value)
    }

    const submit = (e) => {
        e.preventDefault();
        if(!registerMode){
            history.push("/home");
        }
        else{

        }
    }

    return (
        <Grid container className={classes.container}>
            <svg className={classes.topSvgStyle}id="line-box" viewBox="0 0 500 500" preserveAspectRatio="none">
                <defs>
                    <linearGradient id="bulan4" x1="0" y1="0" x2="0" y2="1" gradientTransform="rotate(35)">
                        <stop offset="5%" stopColor="#010d47" stopOpacity={0.8}/>
                        <stop offset="95%" stopColor="#010d47" stopOpacity={0.5}/>
                    </linearGradient>
                </defs>
                <g>
                    <path fill="url(#bulan4)" d="M-55.68,256.74 C1.32,-23.51 553.80,10.03 517.12,-1.80 L501.31,-4.76 L-0.94,-1.80"/>
                </g>
            </svg>
            <svg className={classes.bottomSvgStyle} id="line-box" viewBox="0 0 500 500" preserveAspectRatio="none">
                <defs>
                    <linearGradient id="bulan4" x1="0" y1="0" x2="0" y2="1" gradientTransform="rotate(35)">
                        <stop offset="5%" stopColor="#010d47" stopOpacity={0.8}/>
                        <stop offset="95%" stopColor="#010d47" stopOpacity={0.5}/>
                    </linearGradient>
                </defs>
                <g>
                    <path fill="url(#bulan4)" d="M-55,500 C80,495 356.28,480 500.75,390 L500.00,500 L345.56,500 Z"/>
                </g>
            </svg>
            <Grid item container sm={7} className={classes.leftContainer}>
                <Grid item container justify="center" alignItems="center" direction="column">
                    <Grid item>
                        <img src={EzmeinLogo} alt="" className={classes.logo}></img>
                    </Grid>
                    <Grid item container>
                        <Grid item xs={1}/>
                        <Grid item xs={11} container direction="column">
                            <Grid item container alignItems="center">
                                <HouseIcon className={classes.materialIcon}/>
                                <Typography variant="h6" className={classes.ezManageTypo}>
                                    Ezly manage your boarding house
                                </Typography>
                            </Grid>
                            <Grid item container alignItems="center">
                                <AccountBalanceWalletIcon className={classes.materialIcon}/>
                                <Typography variant="h6" className={classes.ezManageTypo}>
                                    Ezly manage your Customer's Payment
                                </Typography>
                            </Grid>
                            <Grid item container alignItems="center">
                                <AccessibilityNewIcon className={classes.materialIcon}/>
                                <Typography variant="h6" className={classes.ezManageTypo}>
                                    Ez to use
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item container sm={5} justify="center" direction="column" className={classes.rightContainer}>
                <Grid item>
                    <form>
                        <Grid item container justify="center">
                            <Card className={classes.loginCard}>
                                <Grid container direction="column" spacing={3} alignItems="center" style={{padding:'10px'}}>
                                    <Typography variant="h1" color="initial" className={classes.textColorWhite}>
                                        {registerMode?'Register':'Login'}
                                    </Typography>
                                    {
                                        registerMode && 
                                        <Grid item>
                                            <TextField
                                                className={classes.formTextField}
                                                inputProps={{
                                                    className:`${classes.textColorWhite}`
                                                }}
                                                id="login-name"
                                                label="name"
                                                value={name}
                                                onChange={(e)=>handleName(e)}
                                                variant="outlined"
                                            />
                                        </Grid>
                                    }
                                    <Grid item>
                                        <TextField
                                            className={classes.formTextField}
                                            inputProps={{
                                                className:`${classes.textColorWhite}`
                                            }}
                                            id="login-image"
                                            label="email"
                                            value={email}
                                            onChange={(e)=>handleEmail(e)}
                                            variant="outlined"
                                        />
                                    </Grid>
                                    <Grid item>
                                        <TextField
                                            className={classes.formTextField}
                                            inputProps={{
                                                className:`${classes.textColorWhite}`
                                            }}
                                            id="login-password"
                                            type="password"
                                            label="password"
                                            value={password}
                                            onChange={(e)=>handlePassword(e)}
                                            variant="outlined"
                                        />
                                    </Grid>
                                    <Grid item>
                                        <Grid container alignItems="center">
                                            <Typography variant="subtitle2" className={classes.textColorWhite}>
                                                {registerMode?'Already a user?':'don\'t have an account?'}
                                            </Typography>
                                            <Button variant="text" component={Link} className={classes.textColorWhite} onClick={handleRegisterMode}>
                                                {registerMode?'Login':'Register'}
                                            </Button>
                                        </Grid>
                                    </Grid>
                                    <Grid item>
                                        <Button variant="contained" type="submit" onClick={(e)=>submit(e)}>
                                            {registerMode?'Register':'Login'}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Card>
                        </Grid>
                    </form>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default Login
