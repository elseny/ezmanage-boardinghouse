import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useLocation
} from "react-router-dom";

//components
import MainHeader from './shared/components/Navigation/MainHeader/MainHeader';
import Home from './pages/Home/Home';
import Login from './pages/Login/Login';

function App() {
  
  const notLoginPage = () => {
    return <React.Fragment>
          <MainHeader/>
          <Switch>
            <Route exact path="/home">
              <Home/>
            </Route>
        </Switch>  
      </React.Fragment>
  }
  return (
    <Router>
      <Switch>
        <Route exact path="/login">
          <Login/>
        </Route>
        {
          notLoginPage()
        }
        <Redirect to="/home" />
      </Switch>
    </Router>
  );
}

export default App;
