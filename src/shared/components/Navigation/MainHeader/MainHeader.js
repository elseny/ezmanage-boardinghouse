import React,{useState,useEffect} from 'react';

//mui
import AppBar from '@material-ui/core/AppBar';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import EzmeinLogo from "../../../assets/images/Ezmein Program Kost.jpg";
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';

//react router
import {NavLink} from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
    grow: {
      flexGrow: 1,
    },
    ezImage:{
        height:'60px',
        marginTop:'5px',
        marginBottom:'5px',
        boxShadow:'1px 1px 43px 0px rgba(0,0,0,0.55)'
    },
    appBar:{
        backgroundColor:'#a02526',
    },
    navLinkTypo:{
        color:'#f3f4f5',
        textTransform:'uppercase',
        margin:'0px 7px 0px 7px'
    },
    navLink:{
        textDecoration:'none',
    },
    dateTypo:{
        margin: '0px 25px 0px 25px',
        color:'lightgray',
        fontWeight:'bold',
    },
    navLinkButton:{
        backgroundColor:'#010d47',
        color:'white',
        '&:hover':{
            backgroundColor:'#010d47',
        }
    }
}));

function MainHeader() {

    const getDate = (date) => {
        //Wed Nov 11 2020 19:51:40 GMT+0700 (Western Indonesia Time)
        const separatedDate = date.toDateString().split(' ');
        const DMYOnly = () => {
            let newDate=separatedDate[0]+', ';
            for(let i = 1;i<separatedDate.length;i++){
               newDate+=separatedDate[i]+' ' 
            }
            return newDate;
        }

        const dateString = DMYOnly() + date.getHours() + ':'+ date.getMinutes() + ':' + date.getSeconds();
        
        return dateString;
    }

    const classes = useStyles();
    const [date,setDate] = useState(getDate(new Date()));

    const handleDate = (date) =>{
        setDate(getDate(date));
    }

    useEffect(() => {
        const dateTimeout = setTimeout(()=>handleDate(new Date()),1000)
        return () => {
            clearTimeout(dateTimeout);
        }
    })

    return (
        <React.Fragment>
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <NavLink to="/home" >
                        <img src={EzmeinLogo} alt="" className={classes.ezImage}></img>
                    </NavLink>
                    <div className={classes.grow}></div>
                    <Typography className={classes.dateTypo}>
                        {date}
                    </Typography>
                    <NavLink className={classes.navLink} to="/duepayment">
                        <Typography variant="h6" className={classes.navLinkTypo}>
                            Due Payment
                        </Typography>
                    </NavLink>
                    <NavLink className={classes.navLink} to="/duepayment">
                        <Typography variant="h6" className={classes.navLinkTypo}>
                            Due Payment
                        </Typography>
                    </NavLink>
                    <NavLink className={classes.navLink} to="/login">
                        <Button variant="contained" className={classes.navLinkButton}>
                            Logout
                        </Button>
                    </NavLink>
                </Toolbar>
            </AppBar>
            <Toolbar />
        </React.Fragment>
    )
}

export default MainHeader
