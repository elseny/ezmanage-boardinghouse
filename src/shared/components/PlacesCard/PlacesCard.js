import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
    cardContent:{
        marginTop:0,
        marginBottom:0,
    },
    deleteButton:{
      color:"white",
      backgroundColor:"#a02526",
      '&:hover':{
        backgroundColor:"#a02526",
      }
    },
    manageButton:{
      color:"white",
      backgroundColor:"#010d47",
      '&:hover':{
        backgroundColor:"#010d47",
      }
    }
});

function PlacesCard(props) {

    const classes = useStyles();

    return (
      <Card>
        <CardMedia
        component="img"
        alt="Image not Found!"
        height="200"
        image={props.place.imagepath}
        title={props.place.title}
        />
        <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h5">
                {props.place.title}
            </Typography>
            <Typography variant="body2" color="textSecondary">
                {props.place.description}
            </Typography>
        </CardContent>
        <CardActions>
          <Button size="small" variant="contained" className={classes.manageButton}>
            Manage
          </Button>
          <Button size="small" variant="contained" className={classes.deleteButton}>
            Delete
          </Button>
        </CardActions>
      </Card>
    );
}

export default PlacesCard
